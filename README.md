# Rust

**[Rust 1.26](https://blog.rust-lang.org/2018/05/10/Rust-1.26.html)**

- [ ] Retry [`cargo install rustfmt-nightly`](https://github.com/rust-lang-nursery/rustfmt/issues/2304) with new nightly
- [ ] Check out https://github.com/maekawatoshiki/naglfar and how it does graph references
- [ ] Check out https://github.com/edwin0cheng/unrust
- [ ] Check out https://github.com/OUISRC/Rust-webapp-starter
- [ ] Check out https://hacks.mozilla.org/2018/04/javascript-to-rust-and-back-again-a-wasm-bindgen-tale/
- [ ] Check out https://people.gnome.org/~federico/blog/refactoring-some-repetitive-code-to-a-macro.html and https://danielkeep.github.io/tlborm/book/index.html
- [ ] Check out https://github.com/alex-shapiro/ditto
- [ ] Check out this epicness https://anderspitman.net/2018/04/04/static-react-rust-webapp/
- [ ] Check out https://github.com/toidiu/learn-rust
- [ ] Check out https://stevedonovan.github.io/rust-gentle-intro/readme.html
- [ ] Check out https://steveklabnik.github.io/rust-in-ten-slides/
- [ ] Check out https://rustwasm.github.io/
- [ ] Check out https://github.com/actix/actix
- [ ] Check out https://joshleeb.com/posts/rust-traits-and-trait-objects/
- [ ] Check out https://readrust.net/
- [ ] Check out Fearless Rust Bloggers
- [ ] Check out https://github.com/atom/eon

## Rust Fest 2018

[Official site](https://paris.rustfest.eu/)

## Podcasts

- [New Rustacean](https://newrustacean.com/)
- [Rusty Spike](https://rusty-spike.blubrry.net/)
- [Request for Explanation](https://request-for-explanation.github.io/podcast/)

## The Rust Book

[Here](https://doc.rust-lang.org/book/second-edition/)

## Learning Rust

https://learning-rust.github.io/

Exercises:

- http://exercism.io/languages/rust/about
- https://github.com/carols10cents/rustlings

## The Hello Rust Show

- [Official site](https://hello-rust.show/)
- [GitHub repository](https://github.com/hello-rust/show/)
- [YouTube channel](https://www.youtube.com/channel/UCZ_EWaQZCZuGGfnuqUoHujw)

## [Xi Editor](https://github.com/google/xi-editor)

Contributing to the [Xi-Win](https://github.com/google/xi-win) frontend.
[More on Xi](/post/xi).

## [Servo](https://servo.org/)

A browser engine writted in Rust.
More on Servo:

- [GitLab repository](https://gitlab.com/TomasHubelbauer/bloggo-servo)
- [Bloggo post](http://hubelbauer.net/post/bloggo-servo)

## [Diesel](http://diesel.rs/)

- [ ] Try Diesel out

## [Gotham](https://gotham.rs/)

- [ ] Try Gotham out

## [Rocket](https://rocket.rs/)

- [ ] Try Rocket out

## [Cobalt](https://cobalt-org.github.io/)

- [ ] Try Cobalt out

This is just a static site generator, so not hella exciting, but will be good for learning.

## [Tokio](https://tokio.rs/blog/2018-03-tokio-runtime/)

- [ ] Try Tokio out

## [Cargo](https://github.com/rust-lang/cargo) & 3rd Party Subcommands

Cargo commands: `cargo help`

- `build`
- `check`
- `clean`
- `doc`
- `new`
- `init`
- `run`
- `test`
- `bench`
- `update`
- `search`
- [`src`](https://www.ncameron.org/blog/announcing-cargo-src-beta/)
- `publish`
- `install`
- `uninstall`

3rd Party Subcommands:

- [Cargo Add: `add`](https://github.com/withoutboats/cargo-add) deprecated, use Cargo Edit
- [Cargo Audit: `audit`](https://github.com/RustSec/cargo-audit)
- [Cargo Asm: `asm`](https://github.com/gnzlbg/cargo-asm)
- [Cargo Bloat: `bloat`](https://github.com/RazrFalcon/cargo-bloat)
- [Cargo Edit: `add rm upgrate`](https://github.com/killercup/cargo-edit)
- [Cargo Fuzz: `fuzz`](https://github.com/rust-fuzz/cargo-fuzz)
- [Cargo Graph: `graph`](https://github.com/kbknapp/cargo-graph)
- [Cargo Script: `script`](https://github.com/DanielKeep/cargo-script)
- [Cargo Watch: `watch`](https://github.com/passcod/cargo-watch)
- […and more 3rd party Cargo subcommands](https://github.com/rust-lang/cargo/wiki/Third-party-cargo-subcommands)

## Rust+WebAssembly

See the related section in the WebAssembly post:

- [GitLab](https://gitlab.com/TomasHubelbauer/bloggo-wasm)
- [Bloggo](http://hubelbauer.net/post/wasm)

## Snips

Natural Language Processing in Rust (offline!).

- [GitHub repository](https://github.com/snipsco/snips-nlu-rs)

## Rust loaders for JavaScript bundlers

- [Parcel](https://github.com/koute/parcel-plugin-cargo-web)
- [Browserify](https://github.com/browserify/rustify)
- [WebPack](https://github.com/dflemstr/rust-native-wasm-loader) used by [this](https://github.com/yamafaktory/rust-wasm-webpack/)

## Rust Docker Images

- Unofficial Rust Docker images:
    - [`scorpil/rust`](https://hub.docker.com/r/scorpil/rust/`) - seems well tagged, [supported in Rust forum](https://users.rust-lang.org/t/creating-official-docker-image-for-rust/4165)
    - [`jimmycuadra/rust`](https://hub.docker.com/r/jimmycuadra/rust/) - smaller image sizes
    - Other unofficial Docker images.
- No official Rust Docker image. Yet?

## Free Rust Books

- [Rust By Example](https://rustbyexample.com/print.html) (full book page)
- [Why Rust?](http://www.oreilly.com/programming/free/files/why-rust.pdf)
  - This one has good descriptions and examples of the 3 rules
- [The Rustonomicon](https://doc.rust-lang.org/nomicon/)
  - This is not updated nearly enough, so will contain out-of-date information, but will be interesting once more advanced.
- [The Rust Book](https://doc.rust-lang.org/book/) (both versions)
  - Start by version two, which is shaping up real nice, already read v1 but won't hurt to re-read
- [Rust for Rubyists](http://www.rustforrubyists.com/book/book.html)
  - Not really my turf but maybe I'll start learning me some Ruby, soon, so will be useful to compare
- [Writing an OS in Rust](https://os.phil-opp.com/second-edition/) by Philipp Oppermann

## [Hello, Rust!](https://www.hellorust.com/)

- Source: [GitHub](https://github.com/badboy/hellorust)

## Annotated Bookmarks

- [ ] Annotate something when time allows

## Unannotated Bookmarks

- [ ] Annotate the ones that look worth annotating

- [Asciinema rewrite in Rust](https://github.com/LegNeato/asciinema-rs)
- [Guitar Effects in Rust](http://rickyhan.com/jekyll/update/2018/02/06/rust-guitar-pedal-effects-dsp.html)
- [Writing a Simple Parser in Rust](https://adriann.github.io/rust_parser.html)
- [Persistent collections](http://smallcultfollowing.com/babysteps/blog/2018/02/01/in-rust-ordinary-vectors-are-values/)
- [Async/Await I: Self-Referential Structs](https://boats.gitlab.io/blog/post/2018-01-25-async-i-self-referential-structs/) by [withoutboats](https://boats.gitlab.io/blog/page/about/)
- [Windows API Rust](https://github.com/retep998/winapi-rs)
- [WinRT Rust](https://github.com/contextfree/winrt-rust)
- [proc-macro-hack](https://github.com/dtolnay/proc-macro-hack)
- [Find Rust work](https://www.rustaceans.org/findwork)
- [Rust beginners on Mozilla IRC](http://chat.mibbit.com/?server=irc.mozilla.org)
- [WebRender: a sneak peek into Firefox Quantum's future](https://www.youtube.com/watch?v=oXF-uyPIKcc) by Nicolas Silva
- [Servo & WebRender research at University of Szeged](https://www.youtube.com/watch?v=SK5iEOc57wQ) by Attila Dusnoki
- [Writing an OS in Rust](https://os.phil-opp.com/second-edition/) by Philipp Oppermann
- [Building a fast Electron app with Rust](https://keminglabs.com/blog/building-a-fast-electron-app-with-rust/) by Kevin J. Lynagh
